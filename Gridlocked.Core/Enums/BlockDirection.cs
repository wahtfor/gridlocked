﻿// Author: Alex Leone

namespace Gridlocked.Core.Enums
{
    /// <summary>
    /// For each piece, "Up" means:
    /// - T-Cross = Center piece points up. _|_
    /// - L-Bend = One piece points up, the other piece points right. |_
    /// - Straight-line = Either piece points up. |
    /// - Stub = The one piece points up.
    /// </summary>
    public enum BlockDirection : byte
    {
        Up = 0,
        Right,
        Down,
        Left
    }
}
