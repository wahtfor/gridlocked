﻿// Author: Alex Leone

namespace Gridlocked.Core.Enums
{
    public enum BlockType : byte
    {
        Stub,
        StraightLine,
        LBend,
        TCross
    }
}
