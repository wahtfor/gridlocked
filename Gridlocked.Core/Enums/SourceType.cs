﻿// Author: Alex Leone

namespace Gridlocked.Core.Enums
{
    public enum SourceType
    {
        PowerStation,
        WaterTower
    }
}
