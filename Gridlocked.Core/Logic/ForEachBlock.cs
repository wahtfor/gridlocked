﻿// Author: Alex Leone

using System;

namespace Gridlocked.Core.Logic
{
    public static class ForEachBlock
    {
        public static void Do(byte size, Action<byte, byte> action)
        {
            Do((int) size, action);
        }

        public static void Do(int size, Action<byte, byte> action)
        {
            for (byte y = 0; y < size; y += 1)
            {
                for (byte x = 0; x < size; x += 1)
                {
                    action(x, y);
                }
            }
        }
    }
}
