﻿// Author: Alex Leone

using System;
using System.Collections.Generic;
using System.Linq;

namespace Gridlocked.Core.Logic
{
    public static partial class Get
    {
        internal static T RandomItemFrom<T>(Random rng)
        {
            var enumValues = Enums<T>();

            return RandomItemFrom(enumValues, rng);
        }

        internal static T RandomItemFrom<T>(IEnumerable<T> values, Random rng)
        {
            return values.ElementAt(rng.Next(values.Count()));
        }

        public static T[] Enums<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToArray();
        }
    }
}
