﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using System;

namespace Gridlocked.Core.Logic
{
    public static class LevelCode
    {
        // CODES:
        // ------
        // 1 = Straight line
        // 2 = L-Bend
        // 3 = T-Cross
        //
        // M = Power Station (Stub)
        // N = Power Station (Straight line)
        // O = Power Station (L-Bend)
        // P = Power Station (T-Cross)
        //
        // H = House


        public static string Encode(this Grid grid)
        {
            static string CodeFromBlock(Block block)
            {
                if (block.IsHouse)
                {
                    return "H";
                }

                if (block.Type == BlockType.Stub && block.IsPowerStation)
                {
                    return "M";
                }

                if (block.Type == BlockType.StraightLine)
                {
                    return block.IsPowerStation
                        ? "N"
                        : "1";
                }

                if (block.Type == BlockType.LBend)
                {
                    return block.IsPowerStation
                        ? "O"
                        : "2";
                }

                if (block.Type == BlockType.TCross)
                {
                    return block.IsPowerStation
                        ? "P"
                        : "3";
                }

                throw new ArgumentException("Error: Could not figure out how to encode block.");
            };

            int size = grid.Blocks.GetLength(0);
            string code = "";

            ForEachBlock.Do(size, (x, y) =>
            {
                code += CodeFromBlock(grid.Blocks[x, y]);
            });

            return code;
        }

        public static Grid Decode(this string code)
        {
            static Block BlockFromCode(char codeChar) => codeChar switch
            {
                '1' => new Block(BlockType.StraightLine, false, false),
                '2' => new Block(BlockType.LBend, false, false),
                '3' => new Block(BlockType.TCross, false, false),
                'M' => new Block(BlockType.Stub, true, false),
                'N' => new Block(BlockType.StraightLine, true, false),
                'O' => new Block(BlockType.LBend, true, false),
                'P' => new Block(BlockType.TCross, true, false),
                'H' => new Block(BlockType.Stub, false, true),
                _ => throw new ArgumentException($"Error loading level from code: code {codeChar} not recognized.")
            };

            var sqrt = Math.Sqrt(code.Length);

            byte size = (byte)sqrt;

            if (size != sqrt)
            {
                throw new ArgumentException($"Error loading level from code: code must represent a square grid; this grid has {code.Length} blocks.", nameof(code));
            }

            var grid = new Grid
            {
                Blocks = new Block[size, size],
                IsSolved = false
            };

            ForEachBlock.Do(size, (x, y) =>
            {
                char codeChar = code[y * size + x];
                var block = BlockFromCode(codeChar);

                grid.Blocks[x, y] = block;
            });

            ForEachBlock.Do(size, (x, y) =>
            {
                var block = grid.Blocks[x, y];

                block.ConnectToAdjacentBlocks(grid, x, y);
            });

            return grid;
        }
    }
}
