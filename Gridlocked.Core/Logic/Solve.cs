﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Gridlocked.Core.Logic
{
    public static class Solve
    {
        public static IEnumerator<string> RotateToSolve(this Grid grid)
        {
            while (!grid.IsSolved)
            {
                bool madeSomeChange = false;

                foreach (var block in grid.Blocks)
                {
                    var validDirections = block.ValidDirections();

                    if (validDirections.Length == 0)
                    {
                        yield return $"Error solving grid: Block {block.Type} couldn't find any valid directions to face.";
                        yield break;
                    }
                    else if (validDirections.Length == 1 && !block.IsLocked)
                    {
                        var newDirection = validDirections.Single();

                        bool changedBlockDirection = newDirection != block.Direction;

                        block.Direction = newDirection;
                        grid.LastBlockRotated = block;

                        madeSomeChange = true;

                        block.IsLocked = true;

                        grid.UpdateSolvedState();

                        if (grid.IsSolved)
                        {
                            yield break;
                        }
                        else if (changedBlockDirection)
                        {
                            yield return null;
                        }
                    }
                }

                if (!madeSomeChange)
                {
                    yield return "Error solving grid: The algorithm is stuck.";
                    yield break;
                }
            }

            int size = grid.Blocks.GetLength(0);
            var corners = new Block[]
                {
                    grid.Blocks[0, 0],
                    grid.Blocks[size - 1, 0],
                    grid.Blocks[0, size - 1],
                    grid.Blocks[size - 1, size - 1]
                };

            if (grid.IsSolved && corners.Any(block => block.Type == BlockType.StraightLine || block.Type == BlockType.TCross))
            {
                throw new InvalidOperationException("Error: Solving function is broken.");
            }
        }

        public static bool IsSolvable(this Grid grid, out int steps)
        {
            var solution = grid.RotateToSolve();
            steps = 0;

            while (solution.MoveNext())
            {
                if (solution.Current != null)
                {
#if DEBUG
                    Debug.WriteLine(solution.Current);
#endif
                    return false;
                }

                steps += 1;
            }

            return grid.IsSolved;
        }

        private static BlockDirection[] ValidDirections(this Block block)
        {
            // WHICH DIRECTIONS *NEED* A CONNECTION, OR NO CONNECTION?

            var needsConnection = new Dictionary<BlockDirection, bool?>();
            var allDirections = Get.Enums<BlockDirection>();

            foreach (var direction in allDirections)
            {
                var blockInDirection = block.BlockTo(direction);
                if (blockInDirection == null || blockInDirection.IsLocked)
                {
                    var oppositeDirection = direction.Rotate(2);
                    needsConnection[direction] = blockInDirection.Connected(oppositeDirection);
                }
                else
                {
                    needsConnection[direction] = null;
                }
            }

            // FIND VALID DIRECTIONS FOR THE BLOCK,
            // BASED ON ITS TYPE.

            if (block.Type == BlockType.Stub)
            {
                if (needsConnection.Values.Count(v => v == true) == 1)
                {
                    return needsConnection
                        .Where(kvp => kvp.Value == true)
                        .Select(kvp => kvp.Key)
                        .ToArray();
                }

                return needsConnection
                    .Where(kvp => kvp.Value != false)
                    .Select(kvp => kvp.Key)
                    .ToArray();
            }
            else if (block.Type == BlockType.StraightLine)
            {
                if (needsConnection.Values.Count(v => v == false) >= 1)
                {
                    return needsConnection
                        .Where(kvp => kvp.Value == false)
                        .Select(kvp => kvp.Key.Rotate())
                        .Take(1) // There will always be two valid directions for any straight line.
                        .ToArray();
                }

                return needsConnection
                    .Where(kvp => kvp.Value != false)
                    .Select(kvp => kvp.Key)
                    .ToArray();
            }
            else if (block.Type == BlockType.LBend)
            {
                if (needsConnection.Values.Count(v => v == true) >= 2)
                {
                    return needsConnection
                        .Where(kvp => kvp.Value == true && needsConnection[kvp.Key.Rotate()] == true)
                        .Select(kvp => kvp.Key)
                        .ToArray();
                }

                if (needsConnection.Count(kvp => kvp.Value == false && needsConnection[kvp.Key.Rotate()] == true) >= 1)
                {
                    return needsConnection
                        .Where(kvp => kvp.Value == true)
                        .Select(kvp => kvp.Key)
                        .ToArray();
                }

                if (needsConnection.Count(kvp => kvp.Value == true && needsConnection[kvp.Key.Rotate()] == false) >= 1)
                {
                    return needsConnection
                        .Where(kvp => needsConnection[kvp.Key.Rotate()] == true)
                        .Select(kvp => kvp.Key)
                        .ToArray();
                }

                return needsConnection
                    .Where(kvp => kvp.Value != false && needsConnection[kvp.Key.Rotate()] != false)
                    .Select(kvp => kvp.Key)
                    .ToArray();
            }
            else if (block.Type == BlockType.TCross)
            {
                if (needsConnection.Values.Count(v => v == false) == 1)
                {
                    return needsConnection
                        .Where(kvp => kvp.Value == false)
                        .Select(kvp => kvp.Key.Rotate(2))
                        .ToArray();
                }

                return needsConnection
                    .Where(kvp => kvp.Value != true)
                    .Select(kvp => kvp.Key.Rotate(2))
                    .ToArray();
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}
