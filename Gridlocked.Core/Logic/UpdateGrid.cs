﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Models;
using System;

namespace Gridlocked.Core.Logic
{
    public static class UpdateGrid
    {
        public static BlockDirection Rotate(this BlockDirection direction, int times = 1)
        {
            var directionNum = (int)direction;

            directionNum += times % 4;

            if (directionNum >= 4)
            {
                directionNum -= 4;
            }
            else if (directionNum < 0)
            {
                directionNum += 4;
            }
            
            return (BlockDirection)directionNum;
        }

        public static void Reset(this Grid grid, Random randomizer)
        {
            foreach (var block in grid.Blocks)
            {
                block.Direction = Get.RandomItemFrom<BlockDirection>(randomizer);
                block.IsLocked = false;
            }

            grid.LastBlockRotated = null;

            grid.UpdateSolvedState();
        }

        public static void RotateBlock(this Grid grid, byte x, byte y)
        {
            // Locked blocks can't be rotated.
            var blockToRotate = grid.Blocks[x, y];
            if (blockToRotate.IsLocked)
            {
                return;
            }

            // ROTATE BLOCK
            blockToRotate.Direction = blockToRotate.Direction.Rotate();

            // GRIDLOCKED MODE
            if (grid.Difficulty == Difficulty.Gridlocked
                && grid.LastBlockRotated != null
                && grid.LastBlockRotated != blockToRotate)
            {
                // Lock the last block rotated.
                grid.LastBlockRotated.IsLocked = true;
            }
            grid.LastBlockRotated = blockToRotate;

            grid.UpdateSolvedState();

            if (grid.IsSolved)
            {
                grid.LastBlockRotated = null;
            }
        }

        internal static void UpdatePoweredState(this Grid grid)
        {
            Block powerStation = null;
            int size = grid.Blocks.GetLength(0);

            ForEachBlock.Do(size, (x, y) =>
            {
                var block = grid.Blocks[x, y];
                if (block.IsPowerStation)
                {
                    powerStation = block;
                }
                else
                {
                    block.IsPowered = false;
                }
            });

            if (powerStation == null)
            {
                throw new InvalidOperationException("Error updating grid: Game couldn't find a power station!");
            }

            static void PowerBlock(Block block, bool source = false)
            {
                if (block == null)
                {
                    return;
                }

                if (block.IsPowered && !source)
                {
                    return;
                }
                
                block.IsPowered = true;

                if (block.ConnectedUp() && block.BlockAbove.ConnectedDown())
                {
                    PowerBlock(block.BlockAbove);
                }

                if (block.ConnectedRight() && block.BlockToRight.ConnectedLeft())
                {
                    PowerBlock(block.BlockToRight);
                }

                if (block.ConnectedLeft() && block.BlockToLeft.ConnectedRight())
                {
                    PowerBlock(block.BlockToLeft);
                }

                if (block.ConnectedDown() && block.BlockBelow.ConnectedUp())
                {
                    PowerBlock(block.BlockBelow);
                }
            }

            PowerBlock(powerStation, source: true);

            return;
        }

        internal static void UpdateSolvedState(this Grid grid)
        {
            grid.UpdatePoweredState();

            bool isSolved = true;
            
            foreach (var block in grid.Blocks)
            {
                if (!block.IsPowered || !block.TotallyConnected())
                {
                    isSolved = false;
                    break;
                }
            }

            grid.IsSolved = isSolved;
        }
    }
}
