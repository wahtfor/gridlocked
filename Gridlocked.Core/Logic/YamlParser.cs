﻿// Author: Alex Leone

using System.Linq;

namespace Gridlocked.Core.Logic
{
    public static class YamlParser
    {
        public static string[] ParseLevelList(this string yamlText)
        {
            var levels = yamlText
                .Split("\n")
                .Select(level => level.Trim())
                .Where(level => level.StartsWith("- "))
                .Select(level => level.Substring(2))
                .ToArray();

            return levels;
        }
    }
}
