﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using System;

namespace Gridlocked.Core.Models
{
    public class Block
    {
        public Block(BlockType type, bool isPowerStation = false, bool isHouse = false)
        {
            Type = type;
            IsPowerStation = isPowerStation;
            IsHouse = isHouse;

            powered = IsPowerStation;

            if (IsPowerStation && IsHouse)
            {
                throw new ArgumentException($"Error generating grid: Block of type {Type} tried to be BOTH a power station and a house, which is not allowed.");
            }

            if (IsHouse && Type != BlockType.Stub)
            {
                throw new ArgumentException($"Error generating grid: One of the houses tried to have multiple electrical hookups, which is not allowed.");
            }

            if (Type == BlockType.Stub && !IsHouse && !IsPowerStation)
            {
                throw new ArgumentException($"Error generating grid: One of the blocks tried to be a dead end into nothing, which is not allowed.");
            }
        }

        public BlockType Type { get; private set; }

        public bool IsPowerStation { get; private set; }

        public bool IsHouse { get; private set; }

        public BlockDirection Direction { get; internal set; }

        private bool powered = false;
        public bool IsPowered
        {
            get => powered;
            internal set
            {
                if (IsPowerStation && !value)
                {
                    throw new InvalidOperationException("Error setting power: Game tried to turn off the power station, which is not allowed.");
                }

                powered = value;
            }
        }

        public bool IsLocked { get; internal set; }

        // References

        internal Block BlockAbove { get; set; }
        
        internal Block BlockToRight { get; set; }
        
        internal Block BlockBelow { get; set; }
        
        internal Block BlockToLeft { get; set; }

        public Block BlockTo(BlockDirection direction) => direction switch
        {
            BlockDirection.Up => BlockAbove,
            BlockDirection.Right => BlockToRight,
            BlockDirection.Down => BlockBelow,
            BlockDirection.Left => BlockToLeft,
            _ => throw new ArgumentOutOfRangeException(),
        };

        public override string ToString()
        {
            if (IsHouse)
            {
                return $"House ({Direction})";
            }

            if (IsPowerStation)
            {
                return $"Power Station ({Type}, {Direction})";
            }

            return $"{Type} ({Direction})";
        }
    }
}
