﻿// Author: Alex

using Gridlocked.Core.Enums;

namespace Gridlocked.Core.Models
{
    public class Grid
    {
        public Block[,] Blocks;

        public Difficulty Difficulty { get; set; }

        public bool IsSolved { get; internal set; }

        public Block LastBlockRotated { get; internal set; }
    }
}
