﻿using Gridlocked.MonoGame.Core;
using System;

namespace Gridlocked.Desktop
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using var game = new TheGame();
#if DEBUG
            game.Window.IsBorderless = true;
#endif
            game.Run();
        }
    }
}
