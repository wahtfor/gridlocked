﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gridlocked.LevelGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            const int max_number_of_levels = 500;

            GenerateLevels(5, max_number_of_levels);
            GenerateLevels(6, max_number_of_levels);
            GenerateLevels(7, max_number_of_levels);
            GenerateLevels(8, max_number_of_levels);
            GenerateLevels(9, max_number_of_levels);

            Console.ReadKey();
        }

        private static void GenerateLevels(int size, int levelsToGenerate)
        {
            var startedTime = DateTime.Now;

            Console.WriteLine($"Generating {size}x{size} levels... started at {startedTime.ToShortTimeString()}");

            int bad = 0;
            int good = 0;

            // GENERATE LEVELS
            var levels = new HashSet<string>();
            var randomizer = new Random(0);
            while (levels.Count < levelsToGenerate)
            {
                var candidate = Generate.Grid(size, randomizer, out string errorMessage);

                if (errorMessage == null)
                {
                    var levelCode = candidate.Encode();

                    levels.Add(levelCode);
                    good += 1;
                }
                else
                {
                    //Console.WriteLine($"Error generating grid: {errorMessage}");
                    bad += 1;
                }

                //Console.WriteLine($"Generating levels: BAD: {bad}, GOOD: {good}");
            }

            Console.WriteLine($"Generated levels: BAD: {bad}, GOOD: {good}");

            // ORDER ALL LEVELS BY "DIFFICULTY" (WIP?)

            var allDirections = Get.Enums<BlockDirection>();

            var totalInternalTCrosses = new Dictionary<int, int>();
            var totalNumSteps = new Dictionary<int, int>();
            void AddTotalNumSteps(int numSteps)
            {
                if (!totalNumSteps.ContainsKey(numSteps))
                {
                    totalNumSteps[numSteps] = 0;
                }

                totalNumSteps[numSteps] += 1;
            }

            int levelNumber = 0;
            var levelsFromEasiestToHardest = levels.ToArray()
                .Select(levelCode => levelCode.Decode())
                .OrderBy(level =>
                {
                    // SET "STEPS"

                    levelNumber += 1;
                    level.Reset(new Random(levelNumber));

                    if (!level.IsSolvable(out int steps))
                    {
                        //Console.WriteLine($"Level {levelNumber}: Not solvable (?!)");

                        AddTotalNumSteps(int.MaxValue);

                        return int.MaxValue;
                    }

                    //Console.WriteLine($"Level {levelNumber}: {steps} steps to solve");
                    AddTotalNumSteps(steps);

                    // SET T-CROSSES

                    int numInternalTCrosses = 0;

                    foreach (var block in level.Blocks)
                    {
                        if (block.Type == BlockType.TCross
                            && allDirections.All(direction => block.BlockTo(direction) != null))
                        {
                            numInternalTCrosses += 1;
                        }
                    }

                    //Console.WriteLine($"Found {numInternalTs} internal T-crosses.");

                    if (!totalInternalTCrosses.ContainsKey(numInternalTCrosses))
                    {
                        totalInternalTCrosses[numInternalTCrosses] = 0;
                    }
                    totalInternalTCrosses[numInternalTCrosses] += 1;

                    return steps + (numInternalTCrosses * 2); // Multiply by 1-4
                })
                .Select(level => level.Encode())
                .ToArray();


            //foreach (var numSteps in totalNumSteps.OrderBy(kvp => kvp.Key))
            //{
            //    Console.WriteLine($"{numSteps.Key} steps to solve: {numSteps.Value} levels");
            //}

            //foreach (var tCross in totalInternalTCrosses.OrderBy(kvp => kvp.Key))
            //{
            //    Console.WriteLine($"{tCross.Key} T-Crosses: {tCross.Value} levels");
            //}

            // TAKE THE 10 LEAST DIFFICULT LEVELS (SHUFFLED),
            // FOLLOWED BY THE REST OF THE LEVELS (SHUFFLED).

            var allLevels = levelsFromEasiestToHardest
                .Take(10)
                .OrderBy(l => randomizer.Next())
                .Concat(
                    levelsFromEasiestToHardest
                        .Skip(10)
                        .OrderBy(l => randomizer.Next())
                )
                .ToArray();

            // WRITE FINAL LEVEL LIST TO A FILE.
            var allLevelsFileText = "- " + string.Join("\n- ", allLevels);
            File.WriteAllText($"{size}x{size}Levels.yaml", allLevelsFileText);

            var endedTime = DateTime.Now;
            Console.WriteLine($"DONE. Generated {levelsToGenerate} {size}x{size} levels.");
            Console.WriteLine($"Started at {startedTime.ToShortTimeString()}, ended at {endedTime.ToShortTimeString()}.");
            Console.WriteLine($"Took {(endedTime - startedTime)} to complete.");
            Console.WriteLine("-----------------");
            //Console.ReadKey();
        }
    }
}
