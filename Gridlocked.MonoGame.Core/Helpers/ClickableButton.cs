﻿// Author: Alex Leone

using MonoGame.Extended;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class ClickableButton
    {
        internal RectangleF Rect { get; set; }

        internal object Data { get; set; }

        internal bool Highlighted { get; set; }

        internal bool Disabled { get; set; }
    }
}
