﻿// Author: Alex Leone

using Microsoft.Xna.Framework;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class Colors
    {
        internal static Color Powered => new Color(255, 255, 40);

        internal static Color Flowing => new Color(71, 214, 255);

        internal static Color UnConnected => new Color(128, 128, 128);

        internal static Color Highlighted => new Color(64, 64, 64);
        
        internal static Color Grid => new Color(32, 32, 32);
        
        internal static Color Background => new Color(16, 16, 16);

        internal static Color Lock => new Color(192, 192, 192) * 0.5F;

        internal static Color ButtonDown => new Color(240, 240, 240);
    }
}
