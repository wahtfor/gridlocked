﻿// Author: Alex Leone

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class DrawHelpers
    {
        internal static Vector2 ToVector2(this Point2 point)
        {
            return new Vector2(point.X, point.Y);
        }

        internal static void DrawCenteredText(this SpriteBatch spriteBatch, string text, SpriteFont spriteFont, Vector2 centeredPoint, Color color, float layerDepth = Layers.Base)
        {
            var textSize = spriteFont.MeasureString(text);

            var position = centeredPoint - textSize / 2;
            
            spriteBatch.DrawString(
                spriteFont,
                text,
                position,
                color,
                layerDepth: layerDepth
            );
        }

        internal static void Draw(this SpriteBatch spriteBatch, TopBar topBar, Textures textures, Fonts fonts)
        {
            var arrowColor = topBar.BackButton.Highlighted
                ? Color.White
                : Colors.ButtonDown;

            var arrowRect = topBar.BackButton.Rect.ToRectangle();
            arrowRect.Inflate(-6, -6);

            spriteBatch.Draw(
                textures.BackArrow,
                arrowRect,
                arrowColor,
                layerDepth: 0.15F
            );

            var topBarFont = fonts.RegularText;

            var textSize = topBarFont.MeasureString(topBar.Text);

            const int screenWidth = 300;
            var textPosition = new Vector2(screenWidth / 2, arrowRect.Center.Y) - (textSize / 2);

            spriteBatch.DrawString(
                topBarFont,
                topBar.Text,
                textPosition,
                Color.White,
                layerDepth: 0.15F
            );
        }

        internal static void DrawString(this SpriteBatch spriteBatch, SpriteFont spriteFont, string text, Vector2 position, Color color, float layerDepth = Layers.Base)
        {
            spriteBatch.DrawString(
                spriteFont,
                text,
                position,
                color,
                rotation: 0,
                origin: Vector2.Zero,
                scale: 1,
                SpriteEffects.None,
                layerDepth: layerDepth
            );
        }

        internal static void Draw(this SpriteBatch spriteBatch, Texture2D texture, RectangleF targetRect, Color color, float layerDepth = Layers.Base)
        {
            spriteBatch.Draw(texture, targetRect.ToRectangle(), color, layerDepth);
        }

        internal static void Draw(this SpriteBatch spriteBatch, Texture2D texture, Rectangle targetRect, Color color, float layerDepth = Layers.Base)
        {
            spriteBatch.Draw(
                texture,
                targetRect,
                sourceRectangle: null,
                color,
                rotation: 0,
                origin: Vector2.Zero,
                SpriteEffects.None,
                layerDepth: layerDepth
            );
        }

        internal static void DrawButton(this SpriteBatch spriteBatch, ClickableButton button, SpriteFont font, float layerDepth = Layers.Base)
        {
            spriteBatch.DrawRectangle(
                button.Rect,
                Color.White,
                thickness: button.Highlighted ? 3 : 2,
                layerDepth: layerDepth
            );

            spriteBatch.DrawCenteredText(
                button.Data.ToString(),
                font,
                button.Rect.Center.ToVector2(),
                Color.White,
                layerDepth: layerDepth
            );
        }

        internal static void DrawModal(this SpriteBatch spriteBatch, ClickableButton modal, string message, Rectangle gameBounds, Fonts fonts, params ClickableButton[] buttons)
        {
            // TODO: Figure out a way to draw over the WHOLE screen here.
            // The edges are only a LITTLE noticeable, and only if you're looking, so it's probably fine.
            spriteBatch.FillRectangle(gameBounds, Colors.Background * 0.6F, layerDepth: 0.06F);

            spriteBatch.FillRectangle(
                modal.Rect,
                Colors.Background,
                layerDepth: 0.05F
            );
            spriteBatch.DrawRectangle(
                modal.Rect,
                Color.White,
                thickness: 2,
                layerDepth: 0F
            );

            const int margin = 10;
            spriteBatch.DrawString(
                fonts.RegularText,
                message,
                (modal.Rect.ToRectangle().Location + new Point(margin * 3)).ToVector2(),
                Color.White,
                layerDepth: 0F
            );

            foreach (var button in buttons)
            {
                spriteBatch.DrawButton(button, fonts.RegularText, Layers.PopUp);
            }
        }
    }
}
