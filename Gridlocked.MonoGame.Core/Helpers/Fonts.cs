﻿// Author: Alex Leone

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class Fonts
    {
        internal Fonts(ContentManager content)
        {
            TitleText = content.Load<SpriteFont>("Fonts/TitleText");
            RegularText = content.Load<SpriteFont>("Fonts/RegularText");
            LevelButtonText = content.Load<SpriteFont>("Fonts/LevelButtonText");
        }

        internal SpriteFont TitleText { get; private set; }
        internal SpriteFont RegularText { get; private set; }
        internal SpriteFont LevelButtonText { get; private set; }
    }
}
