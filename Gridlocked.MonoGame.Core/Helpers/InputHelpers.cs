﻿// Author: Alex Leone

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class InputHelpers
    {
        internal static Point TransformPosition(this Point point, Matrix matrix)
        {
            var vector = point.ToVector2();

            var transformedVector = Vector2.Transform(vector, Matrix.Invert(matrix));

            return transformedVector.ToPoint();
        }

        internal static (
            bool wasInputJustDown,
            bool isInputDown,
            bool wasInputJustUp,
            Point rawInputPosition,
            Point dragDistance
            ) GetInputState(this MouseStateExtended mouseState, TouchCollection touches, Matrix matrix)
        {
            if (touches.Count > 0)
            {
                var touch = touches.Last();

                var rawPosition = touch.Position.ToPoint();
                var dragDistance = Point.Zero;
                if (touch.TryGetPreviousLocation(out var previousTouch))
                {
                    dragDistance = previousTouch.Position.ToPoint() - rawPosition;
                }

                dragDistance = TransformPosition(dragDistance, matrix);

                return
                (
                    touch.State == TouchLocationState.Pressed,
                    isInputDown: touch.State != TouchLocationState.Released,
                    touch.State == TouchLocationState.Released,
                    rawPosition,
                    dragDistance
                );
            }
            else
            {
                var dragDistance = Point.Zero;
                if (mouseState.DeltaScrollWheelValue != 0)
                {
                    const int HEIGHT_OF_LEVEL_SELECT_ROW = 58;
                    var scrollDirection = Math.Sign(mouseState.DeltaScrollWheelValue);
                    dragDistance = new Point(0, HEIGHT_OF_LEVEL_SELECT_ROW * scrollDirection / 2);
                }
                else if (mouseState.IsButtonDown(MouseButton.Left))
                {
                    dragDistance = mouseState.DeltaPosition;
                }

                dragDistance = TransformPosition(dragDistance, Matrix.Invert(matrix));

                return
                (
                    mouseState.WasButtonJustDown(MouseButton.Left),
                    isInputDown: mouseState.IsButtonDown(MouseButton.Left),
                    mouseState.WasButtonJustUp(MouseButton.Left),
                    mouseState.Position,
                    dragDistance
                );
            }
        }

        internal static object[] ProcessButtonClicks(
            this MouseStateExtended mouseState,
            TouchCollection touches,
            Matrix matrix,
            Point offset,
            IEnumerable<ClickableButton> buttons
        )
        {
            (
                bool wasInputJustDown,
                bool isInputDown,
                bool wasInputJustUp,
                Point rawInputPosition,
                Point dragDistance
            ) = mouseState.GetInputState(touches, matrix);

            var inputPosition = TransformPosition(rawInputPosition - offset, matrix);

            if (wasInputJustDown)
            {
                var buttonsClicked = buttons.Where(level => level.Rect.Contains(inputPosition));

                foreach (var button in buttonsClicked.Where(button => !button.Disabled))
                {
                    button.Highlighted = true;
                }
            }
            else if (wasInputJustUp)
            {
                var buttonReleased = buttons
                    .Where(level => level.Highlighted && level.Rect.Contains(inputPosition))
                    .ToArray();

                if (buttonReleased.Any())
                {
                    foreach (var button in buttonReleased)
                    {
                        button.Highlighted = false;
                    }

                    return buttonReleased.Select(button => button.Data).ToArray();
                }
                else
                {
                    return new object[] { null };
                }
            }
            else if (dragDistance != Point.Zero)
            {
                foreach (var button in buttons)
                {
                    if (!button.Rect.Contains(inputPosition))
                    {
                        button.Highlighted = false;
                    }
                }
            }

            return Array.Empty<object>();
        }
    }
}
