﻿// Author: Alex Leone

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class Layers
    {
        internal const float PopUp = 0F;
        internal const float Grid = 0.1F;
        internal const float Overlay = 0.4F;
        internal const float Base = 0.5F;
        internal const float Background = 1F;
    }
}
