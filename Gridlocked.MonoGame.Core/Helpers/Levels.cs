﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using System.Collections.Generic;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class Levels
    {
        internal Dictionary<Difficulty, int> MaxLevelsBeaten { get; set; }

        internal Dictionary<Difficulty, int> MaxLevelsUnlocked { get; set; }

        internal string[] EncodedLevels { get; set; }
    }
}
