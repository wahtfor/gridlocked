﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal static class Save
    {
#if !ANDROID
        private const string COMPANY_NAME = "Leone Gaming";
        private const string GAME_NAME = "Gridlocked";
#endif
        private const string FILE_NAME = "Gridlocked.save";

        internal static void Progress(Dictionary<byte, Levels> levelPacks, SourceType currentSourceType, Difficulty currentDifficulty, bool hasViewedTutorial)
        {
            var saveData = new SaveData
            {
                Progress = levelPacks
                    .ToDictionary(kvp => kvp.Key, kvp => new LevelPackData
                    {
                        MaxLevelsBeaten = kvp.Value.MaxLevelsBeaten,
                        MaxLevelsUnlocked = kvp.Value.MaxLevelsUnlocked
                    }),
                CurrentDifficulty = currentDifficulty,
                CurrentSourceType = currentSourceType,
                HasViewedTutorial = hasViewedTutorial
            };

            var serializedSaveData = JsonConvert.SerializeObject(saveData);

            var filePath = Path.Combine(GetSaveDirectory(), FILE_NAME);
            File.WriteAllText(filePath, serializedSaveData);
        }

        /// <summary>
        /// Call AFTER loading the levels into the level packs.
        /// </summary>
        internal static void Load(Dictionary<byte, Levels> levelPacks, out Difficulty currentDifficulty, out SourceType currentSourceType, out bool hasViewedTutorial)
        {
            var filePath = Path.Combine(GetSaveDirectory(), FILE_NAME);
            
            if (!File.Exists(filePath))
            {
                currentDifficulty = Difficulty.Normal;
                currentSourceType = SourceType.PowerStation;
                hasViewedTutorial = false;

                foreach (byte size in Enumerable.Range(5, 5))
                {
                    levelPacks[size].MaxLevelsBeaten = new Dictionary<Difficulty, int>
                    {
                        { Difficulty.Normal, 0 },
                        { Difficulty.Gridlocked, 0 },
                    };
                    levelPacks[size].MaxLevelsUnlocked = new Dictionary<Difficulty, int>
                    {
                        { Difficulty.Normal, 100 },
                        { Difficulty.Gridlocked, 100 },
                    };
                }

                return;
            }

            var serializedSaveData = File.ReadAllText(filePath);

            var saveData = JsonConvert.DeserializeObject<SaveData>(serializedSaveData);

            foreach (byte size in Enumerable.Range(5, 5))
            {
                levelPacks[size].MaxLevelsBeaten = saveData.Progress[size].MaxLevelsBeaten;
                levelPacks[size].MaxLevelsUnlocked = saveData.Progress[size].MaxLevelsUnlocked;
            }

            currentDifficulty = saveData.CurrentDifficulty;
            currentSourceType = saveData.CurrentSourceType;
            hasViewedTutorial = saveData.HasViewedTutorial;
        }

        private class SaveData
        {
            // THESE PROPERTIES ARE PUBLIC SO THAT
            // NEWTONSOFT.JSON CAN SERIALIZE THEM.

            public Dictionary<byte, LevelPackData> Progress { get; set; }

            public Difficulty CurrentDifficulty { get; set; }
            public SourceType CurrentSourceType { get; set; }
            public bool HasViewedTutorial { get; set; }
        }

        private class LevelPackData
        {
            public Dictionary<Difficulty, int> MaxLevelsBeaten { get; set; }

            public Dictionary<Difficulty, int> MaxLevelsUnlocked { get; set; }
        }

        private static string GetSaveDirectory()
        {
#if ANDROID
            var saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
#else
            var saveDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), COMPANY_NAME, GAME_NAME);
#endif

            if (!Directory.Exists(saveDirectory))
            {
                Directory.CreateDirectory(saveDirectory);
            }

            return saveDirectory;
        }
    }
}
