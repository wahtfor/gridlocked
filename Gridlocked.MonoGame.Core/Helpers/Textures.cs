﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class Textures
    {
        internal Textures(ContentManager content)
        {
#pragma warning disable IDE0028 // Simplify collection initialization
            var blockTextures = new Dictionary<BlockType, Texture2D>();
#pragma warning restore IDE0028 // Simplify collection initialization
            blockTextures[BlockType.Stub] = content.Load<Texture2D>("Graphics/stub");
            blockTextures[BlockType.StraightLine] = content.Load<Texture2D>("Graphics/straight-line");
            blockTextures[BlockType.LBend] = content.Load<Texture2D>("Graphics/l-bend");
            blockTextures[BlockType.TCross] = content.Load<Texture2D>("Graphics/t-cross");
            Blocks = blockTextures;

            House = content.Load<Texture2D>("Graphics/house");
            PowerStation = content.Load<Texture2D>("Graphics/power-station");
            WaterTower = content.Load<Texture2D>("Graphics/water-tower");
            Lock = content.Load<Texture2D>("Graphics/lock");
            Unlock = content.Load<Texture2D>("Graphics/unlock");
            BackArrow = content.Load<Texture2D>("Graphics/back-arrow");
            ResetArrow = content.Load<Texture2D>("Graphics/reset-arrow");
            LightningBolt = content.Load<Texture2D>("Graphics/lightning-bolt");
            WaterDrop = content.Load<Texture2D>("Graphics/water-drop");
            TutorialIcon = content.Load<Texture2D>("Graphics/tutorial");
        }

        internal IReadOnlyDictionary<BlockType, Texture2D> Blocks { get; private set; }
        internal Texture2D House { get; private set; }
        internal Texture2D PowerStation { get; private set; }
        internal Texture2D WaterTower { get; private set; }
        internal Texture2D Lock { get; private set; }
        internal Texture2D Unlock { get; private set; }
        internal Texture2D BackArrow { get; private set; }
        internal Texture2D ResetArrow { get; private set; }
        internal Texture2D LightningBolt { get; private set; }
        internal Texture2D WaterDrop { get; private set; }
        internal Texture2D TutorialIcon { get; private set; }
    }
}
