﻿// Author: Alex Leone

using Microsoft.Xna.Framework;

namespace Gridlocked.MonoGame.Core.Helpers
{
    internal class TopBar
    {
        public TopBar(string leftText)
        {
            Text = leftText;

            BackButton = new ClickableButton
            {
                Rect = new Rectangle(5 + 6, 5 + 6, 48 - 12, 48 - 12),
                Data = "Back"
            };
        }

        internal ClickableButton BackButton { get; private set; }

        internal string Text { get; private set; }
    }
}
