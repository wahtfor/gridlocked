﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Logic;
using Gridlocked.Core.Models;
using Gridlocked.Core.Resources;
using Gridlocked.MonoGame.Core.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended;
using MonoGame.Extended.Input;
using MonoGame.Extended.Screens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Gridlocked.MonoGame.Core.Screens
{
    public class GridScreen : GameScreen
    {
        private new TheGame Game => (TheGame)base.Game;

        private readonly Grid grid;
        private readonly SourceType sourceType;

#if DEBUG
        private IEnumerator<string> solution;
        private const float solutionDelay = 0.2F;
        private double lastSolveStepTime = 0;
#endif

        private readonly byte size;
        private readonly int level;

        private readonly TopBar topBar;
        private readonly ClickableButton[] blockButtons;
        private readonly ClickableButton nextLevelButton;
        private readonly ClickableButton resetButton;
        
        private readonly ClickableButton tutorialButton;

        private readonly ClickableButton tutorialModal;
        private readonly ClickableButton okayButton;

        private bool tutorialVisible = false;

        internal GridScreen(TheGame game, SourceType sourceType, byte size, Difficulty difficulty, int level) : base(game)
        {
            this.sourceType = sourceType;
            this.size = size;
            this.level = level;

            grid = Game.Levels[size].EncodedLevels[level - 1].Decode();
            grid.Reset(new Random(level)); // Shuffling, but always the same for each level.

            grid.Difficulty = difficulty;

            topBar = new TopBar(string.Format(Strings.LevelsBeatenOverUnlocked, level, game.Levels[size].MaxLevelsUnlocked[difficulty]));

            tutorialButton = new ClickableButton
            {
                Rect = new RectangleF(
                    new Point2(
                        Game.Bounds.Width - topBar.BackButton.Rect.X - topBar.BackButton.Rect.Width + 3,
                        topBar.BackButton.Rect.Y + 3
                    ),
                    new Size2(
                        topBar.BackButton.Rect.Size.Width - 6,
                        topBar.BackButton.Rect.Size.Height - 6
                    )
                ),
                Data = "T"
            };
            var modalBounds = Game.Bounds;
            modalBounds.Inflate(-30, -55);

            tutorialModal = new ClickableButton
            {
                Rect = modalBounds,
                Data = "Modal"
            };
            okayButton = new ClickableButton
            {
                Rect = new Rectangle(
                    modalBounds.Center.X - 48,
                    modalBounds.Bottom - 10 * 3 - 48,
                    48 * 2,
                    48
                ),
                Data = Strings.Okay
            };

            var blockButtonsList = new List<ClickableButton>();
            ForEachBlock.Do(size, (x, y) =>
            {
                var position = GridOrigin + new Vector2(BlockSize * x, BlockSize * y);

                blockButtonsList.Add(new ClickableButton
                {
                    Rect = new RectangleF(
                        position.X,
                        position.Y,
                        BlockSize,
                        BlockSize
                    ),
                    Data = (x, y)
                });
            });
            blockButtons = blockButtonsList.ToArray();

            const int width = 150;
            const int height = 50;
            const int margin = 20;
            var nextLevelRect = new Rectangle(
                (GridOrigin + new Vector2((GridWidthOrHeight - width) / 2, GridWidthOrHeight + margin)).ToPoint(),
                new Point(width, height)
            );
            nextLevelButton = new ClickableButton
            {
                Rect = nextLevelRect,
                Data = LastLevelInBlock
                    ? Strings.Return
                    : Strings.NextLevel
            };

            resetButton = new ClickableButton
            {
                Rect = nextLevelRect,
                Data = "Reset"
            };

            if (!Game.HasViewedTutorial)
            {
                tutorialVisible = true;
            }
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = MouseExtended.GetState();
            var touches = TouchPanel.GetState();
            var matrix = Resolution.GetTransformationMatrix();

            if (tutorialVisible)
            {
                var modalClickResults = mouseState.ProcessButtonClicks(
                    touches,
                    matrix,
                    offset: Game.InputOffset,
                    new[] { okayButton, tutorialModal }
                );

                if (modalClickResults.Contains(okayButton.Data)
                    || (modalClickResults.Any() && !modalClickResults.Contains(tutorialModal.Data)))
                {
                    tutorialVisible = false;
                    Game.HasViewedTutorial = true;

                    Save.Progress(Game.Levels, sourceType, grid.Difficulty, hasViewedTutorial: true);
                }

                return;
            }

#if DEBUG
            // ADVANCE SOLUTION ATTEMPT
            if (solution != null && lastSolveStepTime + solutionDelay < gameTime.TotalGameTime.TotalSeconds)
            {
                if (solution.MoveNext())
                {
                    if (solution.Current != null)
                    {
                        Debug.WriteLine(solution.Current);
                    }

                    lastSolveStepTime = gameTime.TotalGameTime.TotalSeconds;
                }
                else
                {
                    solution = null;
                }
            }
#endif

            // ACCEPT GRID AND BUTTON CLICKS
            var clickResults = mouseState.ProcessButtonClicks(
                touches,
                matrix,
                offset: Game.InputOffset,
                blockButtons.Concat(new[] { topBar.BackButton, nextLevelButton, resetButton, tutorialButton })
            );
            if (clickResults.Contains(topBar.BackButton.Data))
            {
                Game.LoadScreen(new LevelSelectScreen(Game, sourceType, size, grid.Difficulty));
            }
            else if (!grid.IsSolved && clickResults.Contains(tutorialButton.Data))
            {
                tutorialVisible = true;
            }
            else if (grid.IsSolved && clickResults.Contains(nextLevelButton.Data))
            {
                if (LastLevelInBlock)
                {
                    Game.LoadScreen(new LevelSelectScreen(Game, sourceType, size, grid.Difficulty));
                }
                else
                {
                    Game.LoadScreen(new GridScreen(Game, sourceType, size, grid.Difficulty, level + 1));
                }
            }
            else if (!grid.IsSolved && clickResults.Contains(resetButton.Data))
            {
                grid.Reset(new Random(level));
            }
            else if (!grid.IsSolved && clickResults.Any(r => r != null))
            {
                var (x, y) = ((byte, byte)) clickResults.FirstOrDefault(r => r != null);

                grid.RotateBlock(x, y);

                if (grid.IsSolved)
                {
                    Game.Levels[size].MaxLevelsBeaten[grid.Difficulty] = Math.Max(level, Game.Levels[size].MaxLevelsBeaten[grid.Difficulty]);

                    Save.Progress(Game.Levels, sourceType, grid.Difficulty, Game.HasViewedTutorial);
                }
            }
        }

        private bool LastLevelInBlock => level == Game.Levels[size].MaxLevelsUnlocked[grid.Difficulty];

        protected Vector2 GridOrigin => GridRect.Location.ToVector2();
        protected Rectangle GridRect => new Rectangle(0, (ScreenHeight - GridWidthOrHeight) / 2, ScreenWidth, ScreenWidth);

        protected int GridWidthOrHeight => ScreenWidth;
        protected int ScreenWidth => Game.Bounds.Width;
        protected int ScreenHeight => Game.Bounds.Height;
        protected int GridSize => grid.Blocks.GetLength(0);
        protected float BlockSize => (float) ScreenWidth / GridSize;

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Colors.Background);

            // DRAW GRID OUTLINES
            {
                int gridThickness = 3;

                Game.SpriteBatch.DrawRectangle(
                    location: GridRect.Location.ToVector2(),
                    GridRect.Size,
                    Colors.Grid,
                    thickness: gridThickness,
                    layerDepth: Layers.Grid
                );

                if (!grid.IsSolved)
                {
                    // Vertical lines
                    for (byte x = 1; x < GridSize; x += 1)
                    {
                        Game.SpriteBatch.DrawLine(
                            GridOrigin + new Vector2(BlockSize * x, 0),
                            GridOrigin + new Vector2(BlockSize * x, GridWidthOrHeight),
                            Colors.Grid,
                            thickness: gridThickness,
                            layerDepth: Layers.Grid
                        );
                    }

                    // Horizontal lines
                    for (byte y = 1; y < GridSize; y += 1)
                    {
                        Game.SpriteBatch.DrawLine(
                            GridOrigin + new Vector2(0, BlockSize * y),
                            GridOrigin + new Vector2(GridWidthOrHeight, BlockSize * y),
                            Colors.Grid,
                            thickness: gridThickness,
                            layerDepth: Layers.Grid
                        );
                    }
                }
            }

            // DRAW BLOCKS
            bool isPowerStation = sourceType == SourceType.PowerStation;
            {
                static float Rotation(BlockDirection direction)
                {
                    return direction switch
                    {
                        BlockDirection.Up => 0,
                        BlockDirection.Right => MathHelper.PiOver2,
                        BlockDirection.Down => MathHelper.Pi,
                        BlockDirection.Left => MathHelper.Pi + MathHelper.PiOver2,
                        _ => throw new ArgumentOutOfRangeException()
                    };
                }

                foreach (var blockButton in blockButtons)
                {
                    var (x, y) = ((byte, byte)) blockButton.Data;
                    var block = grid.Blocks[x, y];
                    var blockTexture = Game.Textures.Blocks[block.Type];
                    var blockColor = block.IsPowered
                        ? (
                            isPowerStation
                                ? Colors.Powered
                                : Colors.Flowing
                          )
                        : Colors.UnConnected;
                    float scale = (float)BlockSize / blockTexture.Width;
                    var origin = blockTexture.Bounds.Center.ToVector2();

                    if (block == grid.LastBlockRotated)
                    {
                        Game.SpriteBatch.FillRectangle(
                            GridOrigin + new Vector2(BlockSize * x, BlockSize * y),
                            new Vector2(BlockSize),
                            Colors.Highlighted,
                            layerDepth: Layers.Background
                        );
                    }

                    void DrawOnBlock(Texture2D texture, Color color, float layerDepth, float rotation = 0)
                    {
                        Game.SpriteBatch.Draw(
                            texture,
                            position: blockButton.Rect.Center.ToVector2(),
                            sourceRectangle: null,
                            color,
                            rotation: rotation,
                            origin: origin,
                            scale: Vector2.One * scale,
                            SpriteEffects.None,
                            layerDepth: layerDepth
                        );
                    }

                    DrawOnBlock(blockTexture, blockColor, layerDepth: 0.9F, rotation: Rotation(block.Direction));

                    if (block.IsHouse || block.IsPowerStation)
                    {
                        var overlayTexture = block.IsHouse
                            ? Game.Textures.House
                            : (
                                isPowerStation
                                    ? Game.Textures.PowerStation
                                    : Game.Textures.WaterTower
                              );

                        DrawOnBlock(overlayTexture, blockColor, layerDepth: Layers.Base);
                    }

                    if (block.IsLocked && !grid.IsSolved)
                    {
                        DrawOnBlock(Game.Textures.Lock, Colors.Lock, layerDepth: Layers.Overlay);
                    }
                }
            }

            // BACK ARROW

            Game.SpriteBatch.Draw(topBar, Game.Textures, Game.Fonts);

            if (grid.IsSolved)
            {
                // NEXT LEVEL
                Game.SpriteBatch.DrawButton(nextLevelButton, Game.Fonts.RegularText);
            }
            else
            {
                // RESET LEVEL

                Game.SpriteBatch.DrawRectangle(
                    resetButton.Rect,
                    Color.White,
                    thickness: resetButton.Highlighted ? 3 : 2,
                    layerDepth: Layers.Base
                );

                const int resetButtonSize = 40;
                var resetRect = resetButton.Rect.ToRectangle();
                Game.SpriteBatch.Draw(
                    Game.Textures.ResetArrow,
                    new Rectangle(
                        resetRect.Location + new Point(resetRect.Width / 2 - resetButtonSize / 2, resetRect.Height / 2 - resetButtonSize / 2),
                        new Point(resetButtonSize)
                    ),
                    Color.White,
                    layerDepth: Layers.Base
                );
            }

            // TUTORIAL (?) BUTTON

            if (!grid.IsSolved)
            {
                Game.SpriteBatch.Draw(
                    Game.Textures.TutorialIcon,
                    tutorialButton.Rect,
                    tutorialButton.Highlighted
                        ? Color.White
                        : Colors.ButtonDown,
                    layerDepth: Layers.Base
                );
            }

            // TUTORIAL

            if (tutorialVisible)
            {
                string action =
#if ANDROID
                    Strings.Tap;
#else
                    Strings.Click;
#endif
                string source = isPowerStation
                    ? Strings.PowerStation
                    : Strings.WaterTower;
                string connector = isPowerStation
                    ? Strings.Wires
                    : Strings.Pipes;

                string tutorialMessage = string.Format(Strings.TutorialText, action, source, connector);
                
                Game.SpriteBatch.DrawModal(
                    tutorialModal,
                    tutorialMessage,
                    Game.Bounds,
                    Game.Fonts,
                    okayButton
                );
            }
        }
    }
}
