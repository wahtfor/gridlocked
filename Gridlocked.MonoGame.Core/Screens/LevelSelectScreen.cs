﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Resources;
using Gridlocked.MonoGame.Core.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended;
using MonoGame.Extended.Input;
using MonoGame.Extended.Screens;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gridlocked.MonoGame.Core.Screens
{
    public class LevelSelectScreen : GameScreen
    {
        private readonly SourceType sourceType;
        private readonly byte size = 5;
        private readonly Difficulty difficulty;
        private readonly float maxOffset = 0;
        private const float dragThreshhold = 5;

        private readonly TopBar topBar;
        private readonly ClickableButton[] levelButtons;

        private double dragOffset = 0;
        private Point? dragStart;
        //private double speed = 0;
        //private const float accelerationDrag = 15;

        internal const int LEVEL_BUTTON_WIDTH = 48;
        internal const int LEVEL_BUTTON_MARGIN = 10;
        
        private int MaxLevelBeaten => Game.Levels[size].MaxLevelsBeaten[difficulty];
        
        private int NextLevel => MaxLevelBeaten + 1;

        private new TheGame Game => (TheGame)base.Game;

        internal LevelSelectScreen(TheGame game, SourceType sourceType, byte size, Difficulty difficulty) : base(game)
        {
            this.sourceType = sourceType;
            this.size = size;
            this.difficulty = difficulty;

            topBar = new TopBar($"{string.Format(Strings.SizeBySize, size)} {Strings.Levels}");

            int totalLevels = Game.Levels[size].MaxLevelsUnlocked[difficulty];
            int level = 1;

            var levelButtonsList = new List<ClickableButton>();
            for (int row = 0; row < totalLevels / 5; row += 1)
            {
                for (byte column = 0; column < 5; column += 1)
                {
                    var buttonRect = new Rectangle(
                        LEVEL_BUTTON_MARGIN + column * (LEVEL_BUTTON_WIDTH + LEVEL_BUTTON_MARGIN),
                        LEVEL_BUTTON_MARGIN * 2 + LEVEL_BUTTON_WIDTH + row * (LEVEL_BUTTON_WIDTH + LEVEL_BUTTON_MARGIN),
                        LEVEL_BUTTON_WIDTH,
                        LEVEL_BUTTON_WIDTH
                    );

                    levelButtonsList.Add(new ClickableButton
                    {
                        Rect = buttonRect,
                        Data = level,
#if DEBUG
                        Disabled = false
#else
                        Disabled = level > NextLevel
#endif
                    });

                    level += 1;
                }
            }
            levelButtons = levelButtonsList.ToArray();

            maxOffset = 500 - (LEVEL_BUTTON_MARGIN * 2 + LEVEL_BUTTON_WIDTH + (totalLevels / 5) * (LEVEL_BUTTON_WIDTH + LEVEL_BUTTON_MARGIN));
            int currentLevelRow = Game.Levels[size].MaxLevelsBeaten[difficulty] / 5 - 2;
            dragOffset = -((LEVEL_BUTTON_MARGIN * 2 + LEVEL_BUTTON_WIDTH + currentLevelRow * (LEVEL_BUTTON_WIDTH + LEVEL_BUTTON_MARGIN)));
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = MouseExtended.GetState();
            var touches = TouchPanel.GetState();

            // Click buttons
            var matrix = Resolution.GetTransformationMatrix();
            var dragOffsetPoint = new Point(0, (int)dragOffset).TransformPosition(Matrix.Invert(matrix));
            var offset = Game.InputOffset;

            var clickResults = mouseState.ProcessButtonClicks(touches, matrix, offset + dragOffsetPoint, levelButtons);
            var backClickResults = mouseState.ProcessButtonClicks(touches, matrix, offset, new[] { topBar.BackButton });
            
            if (backClickResults.Contains(topBar.BackButton.Data))
            {
                Game.LoadScreen(new SizeSelectScreen(Game, sourceType, difficulty));
            }
            else if (clickResults.Any(click => click != null))
            {
                int level = (int) clickResults[0];
                
                Game.LoadScreen(new GridScreen(Game, sourceType, size, difficulty, level));
            }
            
            (
                bool wasInputJustDown,
                bool isInputDown,
                bool wasInputJustUp,
                Point rawInputPosition,
                Point dragDistance
            ) = mouseState.GetInputState(touches, matrix);
            var windowRect = Game.Window.ClientBounds;
            windowRect.Offset(Game.Window.ClientBounds.Location.ToVector2() * -1);
            if (wasInputJustDown)
            {
                dragStart = rawInputPosition;
            }
            else if (
                dragDistance != Point.Zero &&
                windowRect.Contains(rawInputPosition) && // Stop dragging once we get off the edge of the screen.
                (!dragStart.HasValue || Vector2.Distance(rawInputPosition.ToVector2(), dragStart.Value.ToVector2()) >= dragThreshhold)
            )
            {
                dragOffset -= dragDistance.Y;
                //speed += dragDistance.Y * gameTime.ElapsedGameTime.TotalSeconds;

                foreach (var button in levelButtons)
                {
                    button.Highlighted = false;
                }
            }
            else if (!isInputDown)
            {
                dragStart = null;

                //if (Math.Abs(speed) > 0.1F)
                //{
                //    var decelleration = Math.Sign(speed) * (accelerationDrag * gameTime.ElapsedGameTime.TotalSeconds);

                //    if (Math.Abs(decelleration) < Math.Abs(speed))
                //    {
                //        speed -= decelleration;
                //    }
                //    else
                //    {
                //        speed = 0;
                //    }

                //    dragOffset -= speed / gameTime.ElapsedGameTime.TotalSeconds;
                //}
                //else
                //{
                //    speed = 0;
                //}
            }

            if (dragOffset > 0)
            {
                dragOffset = 0;
            }
            else if (dragOffset < maxOffset)
            {
                dragOffset = maxOffset;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Colors.Background);

            foreach (var levelButton in levelButtons)
            {
                int level = (int)levelButton.Data;
                var rawButtonRect = levelButton.Rect.ToRectangle();
                var buttonRect = new Rectangle(
                    rawButtonRect.Location + new Point(0, (int)dragOffset),
                    rawButtonRect.Size
                );

                var color = levelButton.Disabled
                    ? Colors.UnConnected
                    : Color.White;

                if (level == NextLevel)
                {
                    Game.SpriteBatch.FillRectangle(
                        buttonRect,
                        Colors.Highlighted,
                        layerDepth: Layers.Background
                    );
                }

                Game.SpriteBatch.DrawRectangle(
                    buttonRect,
                    color,
                    thickness: levelButton.Highlighted ? 3 : 2,
                    layerDepth: Layers.Base
                );

                Game.SpriteBatch.DrawCenteredText(
                    levelButton.Data.ToString(),
                    Game.Fonts.LevelButtonText,
                    buttonRect.Center.ToVector2(),
                    color,
                    layerDepth: Layers.Base
                );
            }

            Game.SpriteBatch.FillRectangle(0, 0, Game.Bounds.Width, 58, Colors.Background, layerDepth: Layers.Overlay);
            Game.SpriteBatch.Draw(topBar, Game.Textures, Game.Fonts);
        }
    }
}
