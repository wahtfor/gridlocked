﻿// Author: Alex Leone

using Gridlocked.Core.Enums;
using Gridlocked.Core.Resources;
using Gridlocked.MonoGame.Core.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended;
using MonoGame.Extended.Input;
using MonoGame.Extended.Screens;
using System;
using System.Linq;

namespace Gridlocked.MonoGame.Core.Screens
{
    public class SizeSelectScreen : GameScreen
    {
        private readonly TopBar topBar;
        private readonly ClickableButton[] levelPackButtons;
        private readonly ClickableButton[] unlockButtons;
        private readonly ClickableButton difficultyButton;
        private readonly ClickableButton modal;
        private readonly ClickableButton yesButton;
        private readonly ClickableButton noButton;

        private readonly SourceType sourceType;
        private Difficulty difficulty;
        private byte? unlockTheseLevels;

        internal const int LEVEL_BUTTON_WIDTH = 48;
        internal const int LEVEL_BUTTON_MARGIN = 10;

        private new TheGame Game => (TheGame)base.Game;

        private static string GetDifficultyText(Difficulty difficulty)
        {
            return difficulty switch
            {
                Difficulty.Gridlocked => Strings.Gridlocked,
                Difficulty.Normal => Strings.Normal,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        internal SizeSelectScreen(TheGame game, SourceType sourceType, Difficulty difficulty) : base(game)
        {
            this.sourceType = sourceType;
            this.difficulty = difficulty;

            topBar = new TopBar(Strings.SizeSelectHeader);

            bool CanUnlockMore(int packSize)
            {
                int maxLevelsBeaten = Game.Levels[(byte)packSize].MaxLevelsBeaten[difficulty];

                bool canUnlockMore =
                    maxLevelsBeaten == Game.Levels[(byte)packSize].MaxLevelsUnlocked[difficulty]
                    && maxLevelsBeaten < Game.Levels[(byte)packSize].EncodedLevels.Length;

                return canUnlockMore;
            }

            levelPackButtons = Enumerable.Range(5, 5)
                .Select(packSize => new ClickableButton
                {
                    Rect = new Rectangle(
                        LEVEL_BUTTON_MARGIN,
                        (LEVEL_BUTTON_MARGIN + LEVEL_BUTTON_WIDTH) * (packSize - 4),
                        300 - LEVEL_BUTTON_MARGIN * 2 - (CanUnlockMore(packSize) ? LEVEL_BUTTON_WIDTH + LEVEL_BUTTON_MARGIN : 0),
                        LEVEL_BUTTON_WIDTH
                    ),
                    Data = Tuple.Create("Load", (byte)packSize)
                })
                .ToArray();

            unlockButtons = Enumerable.Range(5, 5)
                .Where(CanUnlockMore)
                .Select(packSize => new ClickableButton
                {
                    Rect = new Rectangle(
                        300 - LEVEL_BUTTON_MARGIN - LEVEL_BUTTON_WIDTH,
                        (LEVEL_BUTTON_MARGIN + LEVEL_BUTTON_WIDTH) * (packSize - 4),
                        LEVEL_BUTTON_WIDTH,
                        LEVEL_BUTTON_WIDTH
                    ),
                    Data = Tuple.Create("Unlock", (byte) packSize)
                })
                .ToArray();

            difficultyButton = new ClickableButton
            {
                Rect = new Rectangle(
                    LEVEL_BUTTON_WIDTH,
                    410,
                    300 - LEVEL_BUTTON_WIDTH * 2,
                    LEVEL_BUTTON_WIDTH
                ),
                Data = GetDifficultyText(difficulty)
            };

            var modalBounds = Game.Bounds;
            modalBounds.Inflate(-30, -140);

            modal = new ClickableButton
            {
                Rect = modalBounds,
                Data = "Modal"
            };

            int buttonWidth = 80;
            yesButton = new ClickableButton
            {
                Rect = new Rectangle(
                    modalBounds.Center.X - LEVEL_BUTTON_MARGIN - buttonWidth,
                    modalBounds.Bottom - LEVEL_BUTTON_MARGIN * 3 - LEVEL_BUTTON_WIDTH, // 290
                    buttonWidth,
                    LEVEL_BUTTON_WIDTH
                ),
                Data = Strings.Yes
            };
            noButton = new ClickableButton
            {
                Rect = new Rectangle(
                    yesButton.Rect.ToRectangle().Location + new Point((int) yesButton.Rect.Width + LEVEL_BUTTON_MARGIN * 2, 0),
                    yesButton.Rect.ToRectangle().Size
                ),
                Data = Strings.No
            };
        }


        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = MouseExtended.GetState();
            var touches = TouchPanel.GetState();

            if (unlockTheseLevels != null)
            {
                var clickResults = mouseState.ProcessButtonClicks(
                    touches,
                    Resolution.GetTransformationMatrix(),
                    offset: Game.InputOffset,
                    new[] { yesButton, noButton, modal }
                );
                if (clickResults.Contains(yesButton.Data))
                {
                    Game.Levels[unlockTheseLevels.Value].MaxLevelsUnlocked[difficulty] += 100;
                    Save.Progress(Game.Levels, sourceType, difficulty, Game.HasViewedTutorial);
                    
                    Game.LoadScreen(new SizeSelectScreen(Game, sourceType, difficulty));
                }
                else if (clickResults.Contains(noButton.Data) || (clickResults.Any() && !clickResults.Contains(modal.Data)))
                {
                    unlockTheseLevels = null;
                }
            }
            else
            {
                var clickResults = mouseState.ProcessButtonClicks(
                    touches,
                    Resolution.GetTransformationMatrix(),
                    offset: Game.InputOffset,
                    levelPackButtons.Concat(unlockButtons).Concat(new[] { difficultyButton, topBar.BackButton })
                );
                if (clickResults.Contains(topBar.BackButton.Data))
                {
                    Game.LoadScreen(new HomeScreen(Game, sourceType, difficulty));
                }
                else if (clickResults.Contains(difficultyButton.Data))
                {
                    if (difficulty == Difficulty.Normal)
                    {
                        difficulty = Difficulty.Gridlocked;
                    }
                    else
                    {
                        difficulty = Difficulty.Normal;
                    }

                    Save.Progress(Game.Levels, sourceType, difficulty, Game.HasViewedTutorial);

                    Game.LoadScreen(new SizeSelectScreen(Game, sourceType, difficulty));
                }
                else if (clickResults.Any(r => r != null))
                {
                    var clickResult = (Tuple<string, byte>)clickResults[0];
                    if (clickResult.Item1 == "Load")
                    {
                        Game.LoadScreen(new LevelSelectScreen(Game, sourceType, size: clickResult.Item2, difficulty));
                    }
                    else if (clickResult.Item1 == "Unlock")
                    {
                        unlockTheseLevels = clickResult.Item2;
                    }
                }
            }
        }


        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Colors.Background);

            // DRAW LEVEL PACK BUTTONS

            foreach (var button in levelPackButtons)
            {
                Game.SpriteBatch.DrawRectangle(
                    button.Rect,
                    Color.White,
                    thickness: button.Highlighted ? 3 : 2,
                    layerDepth: Layers.Base
                );

                var packFont = Game.Fonts.RegularText;
                int textMargin = LEVEL_BUTTON_MARGIN * 2;
                byte size = ((Tuple<string, byte>)button.Data).Item2;
                string levelPackName = string.Format(Strings.SizeBySize, size);
                var nameSize = packFont.MeasureString(levelPackName);
                Game.SpriteBatch.DrawString(
                    packFont,
                    levelPackName,
                    button.Rect.ToRectangle().Location.ToVector2() + new Vector2(textMargin, button.Rect.Height / 2 - nameSize.Y / 2),
                    Color.White,
                    layerDepth: Layers.Base
                );

                int levelsBeaten = Game.Levels[size].MaxLevelsBeaten[difficulty];
                int levelsUnlocked = Game.Levels[size].MaxLevelsUnlocked[difficulty];
                string progressText = string.Format(Strings.LevelsBeatenOverUnlocked, levelsBeaten, levelsUnlocked);
                var progressSize = packFont.MeasureString(progressText);
                Game.SpriteBatch.DrawString(
                    packFont,
                    progressText,
                    button.Rect.ToRectangle().Location.ToVector2() + new Vector2(button.Rect.Width - textMargin - progressSize.X, button.Rect.Height / 2 - progressSize.Y / 2),
                    Color.White,
                    layerDepth: Layers.Base
                );
            }

            // DRAW UNLOCK BUTTONS

            foreach (var button in unlockButtons)
            {
                Game.SpriteBatch.DrawRectangle(
                    button.Rect,
                    Color.White,
                    thickness: button.Highlighted ? 3 : 2,
                    layerDepth: Layers.Base
                );

                var lockRect = button.Rect;
                lockRect.Inflate(-8, -8);

                Game.SpriteBatch.Draw(
                    Game.Textures.Unlock,
                    lockRect.ToRectangle(),
                    Color.White,
                    layerDepth: Layers.Base
                );
            }

            // DRAW DIFFICULTY LABEL AND BUTTON

            var difficultyLabelFont = Game.Fonts.RegularText;
            string difficultyLabel = Strings.Difficulty;
            var difficultyLabelSize = difficultyLabelFont.MeasureString(difficultyLabel);
            Game.SpriteBatch.DrawString(
                difficultyLabelFont,
                difficultyLabel,
                difficultyButton.Rect.Center.ToVector2() - difficultyLabelSize / 2 - new Vector2(0, LEVEL_BUTTON_WIDTH),
                Color.White,
                layerDepth: Layers.Base
            );

            Game.SpriteBatch.DrawButton(difficultyButton, Game.Fonts.RegularText);

            // DRAW TOP BAR (Back button, etc.)

            Game.SpriteBatch.Draw(topBar, Game.Textures, Game.Fonts);

            // DRAW UNLOCK NOTICE

            if (unlockTheseLevels != null)
            {
                Game.SpriteBatch.DrawModal(
                    modal,
                    string.Format(Strings.UnlockMessage, unlockTheseLevels),
                    Game.Bounds,
                    Game.Fonts,
                    yesButton,
                    noButton
                );
            }
        }
    }
}
