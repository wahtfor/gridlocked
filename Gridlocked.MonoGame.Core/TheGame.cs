﻿// Author: Alex Leone

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Screens;
using Gridlocked.MonoGame.Core.Helpers;
using Gridlocked.MonoGame.Core.Screens;
using Gridlocked.Core.Logic;
using MonoGame.Extended.Screens.Transitions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
#if ANDROID
using System;
using Android.Content;
#endif

namespace Gridlocked.MonoGame.Core
{
    public class TheGame : Game
    {
        private readonly ScreenManager screenManager = new ScreenManager();

        private GraphicsDeviceManager graphics;

        internal SpriteBatch SpriteBatch { get; private set; }

        internal Textures Textures { get; private set; }
        
        internal Fonts Fonts { get; private set; }

        internal Dictionary<byte, Levels> Levels { get; private set; } = new Dictionary<byte, Levels>();
        internal bool HasViewedTutorial { get; set; } = false;

        internal readonly Rectangle Bounds = new Rectangle(0, 0, 300, 500);

        internal Point InputOffset => GraphicsDevice.Viewport.Bounds.Location;

        public TheGame()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                SupportedOrientations = DisplayOrientation.Portrait | DisplayOrientation.PortraitDown
            };

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Window.Title = "Gridlocked";

            Components.Add(screenManager);
        }

        internal bool LockInput { get; private set; }

#if ANDROID
        public Action<Intent> StartActivity { get; internal set; }
#endif

        internal void LoadScreen(GameScreen screen, bool fade = false)
        {
            Transition transition = null;

            if (fade)
            {
                transition = new BetterFadeTransition(GraphicsDevice, Colors.Background, duration: 10);

                LockInput = true;
                transition.Completed += (o, e) => LockInput = false;

                screenManager.LoadScreen(screen, transition);
            }
            else
            {
                screenManager.LoadScreen(screen);
            }
        }

        protected override void Initialize()
        {
            Resolution.Init(ref graphics);
            Resolution.SetVirtualResolution(Bounds.Width, Bounds.Height);

            void UpdateGameResolution()
            {
#if ANDROID
                graphics.PreferredBackBufferWidth = graphics.GraphicsDevice.DisplayMode.Width;
                graphics.PreferredBackBufferHeight = graphics.GraphicsDevice.DisplayMode.Height;
                graphics.IsFullScreen = true;
#elif DEBUG
                //graphics.PreferredBackBufferWidth = 575;
                //graphics.PreferredBackBufferHeight = 700;
                graphics.PreferredBackBufferWidth = 375;
                graphics.PreferredBackBufferHeight = 625;
                graphics.IsFullScreen = false;
#else
                graphics.PreferredBackBufferWidth = 300;
                graphics.PreferredBackBufferHeight = 500;
                graphics.IsFullScreen = false;
#endif
                Resolution.SetResolution(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, FullScreen: graphics.IsFullScreen);
            }

            UpdateGameResolution();
            this.Window.ClientSizeChanged += (s, e) => UpdateGameResolution();

            Levels LoadResourceText(string resourceName)
            {
#if ANDROID
                string platform = "Android";
#else
                string platform = "Desktop";
#endif
                string fullResourceName = $"Gridlocked.{platform}.Content.Levels.{resourceName}";

                using var stream = this.GetType().Assembly.GetManifestResourceStream(fullResourceName);
                using var reader = new StreamReader(stream);

                string text = reader.ReadToEnd();
                
                var levels = new Levels
                {
                    EncodedLevels = text.ParseLevelList()
                };

                return levels;
            }

            foreach (byte size in Enumerable.Range(5, 5))
            {
                Levels[size] = LoadResourceText($"{size}x{size}Levels.yaml");
            }

            // Creates and returns default save data,
            // if no save data exists yet.
            Save.Load(Levels, out var difficulty, out var sourceType, out var hasViewedTutorial);

            // Creates a save file on game load,
            // so it doesn't take as long to do it later.
            Save.Progress(Levels, sourceType, difficulty, hasViewedTutorial);

            HasViewedTutorial = hasViewedTutorial;

            LoadScreen(new HomeScreen(this, sourceType, difficulty));

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            Textures = new Textures(this.Content);
            Fonts = new Fonts(this.Content);
        }

        protected override void Draw(GameTime gameTime)
        {
            Resolution.BeginDraw();

            SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, transformMatrix: Resolution.GetTransformationMatrix());

            base.Draw(gameTime);

            SpriteBatch.End();
        }
    }
}
