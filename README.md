# Gridlocked - a puzzle game

This is the official repository for Gridlocked, a puzzle game designed for mobile.

- [Official itch.io page](https://leonegaming.itch.io/gridlocked) - with full description
- [Report a bug](https://gitlab.com/Amasa/gridlocked/-/issues)
- [Donate](https://alexjleone.com/projects/donate?p=gridlocked#donate) - support this and future projects
